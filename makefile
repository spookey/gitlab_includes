DIR_VENV	:=	venv
SYS_PY		:=	python3

CMD_PY		:=	$(DIR_VENV)/bin/python3
CMD_PIP		:=	$(DIR_VENV)/bin/pip3
CMD_ISORT	:=	$(DIR_VENV)/bin/isort
CMD_BLACK	:=	$(DIR_VENV)/bin/black
LIB_MARKD	:=	$(DIR_VENV)/lib/python\*/site-packages/markdown/__init__.py

SCRIPT		:=	build.py

.PHONY: help
help:
	@echo "makefile"
	@echo "========"
	@echo
	@echo "make build           build with default arguments"
	@echo
	@echo "make action          run black & isort"


$(DIR_VENV):
	$(SYS_PY) -m venv "$(DIR_VENV)"
	$(CMD_PIP) install -U pip setuptools

$(LIB_MARKD): $(DIR_VENV)
	$(CMD_PIP) install -U markdown

$(CMD_ISORT): $(DIR_VENV)
	$(CMD_PIP) install -U isort

$(CMD_BLACK): $(DIR_VENV)
	$(CMD_PIP) install -U black

.PHONY: build
build: $(LIB_MARKD)
	$(CMD_PY) "$(SCRIPT)"

.PHONY: isort
isort: $(CMD_ISORT)
	$(CMD_ISORT) --line-length=79 --profile=black "$(SCRIPT)"

.PHONY: black
black: $(CMD_BLACK)
	$(CMD_BLACK) --line-length=79 "$(SCRIPT)"

.PHONY: action
action: isort black
