#!/usr/bin/env python3

import argparse
import os
import string
import sys

import markdown

BASE_DIR = os.path.realpath(os.path.dirname(__file__))
ENCODING = "utf-8"
HTML = string.Template(
    """
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>${title}</title>
</head>
<body>
${body}
</body>
</html>
    """.strip()
)


def arguments():
    def _help(txt):
        return f"{txt} | %(default)s"

    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__),
        add_help=True,
        epilog="c[_]",
    )

    parser.add_argument(
        "-i",
        "--intro",
        default=os.path.join(BASE_DIR, "README.md"),
        help=_help("intro file"),
    )
    parser.add_argument(
        "-j",
        "--job",
        default=os.path.join(BASE_DIR, "jobs"),
        help=_help("job directory"),
    )
    parser.add_argument(
        "-e",
        "--extensions",
        nargs="+",
        default=["yml", "yaml"],
        help=_help("job extensions"),
    )
    parser.add_argument(
        "-t",
        "--title",
        default="Gitlab CI includes",
        help=_help("page title"),
    )
    parser.add_argument(
        "-o",
        "--output",
        default=os.path.join(BASE_DIR, "public", "index.html"),
        help=_help("output file"),
    )

    return parser.parse_args()


class Code:
    def __init__(self, *, args):
        self.args = args

    @staticmethod
    def _read(*, source):
        if not os.path.exists(source):
            return
        with open(source, "r", encoding=ENCODING) as handle:
            for line in handle.readlines():
                yield line.rstrip()

    @staticmethod
    def _collect(*, source, extensions):
        if not os.path.exists(source):
            return
        for elem in os.listdir(source):
            if not elem.startswith(os.path.extsep):
                for extension in extensions:
                    if elem.endswith(extension):
                        yield elem, os.path.join(source, elem)

    @staticmethod
    def _gather(*lines):
        desc, code = [], []
        flip = False
        for line in lines:
            if line == "---":
                flip = True
            else:
                keep, _, clean = (
                    ("", "_", line) if flip else line.rpartition("#")
                )
                (code if flip else desc).append(f"{keep}{clean}")
        return desc, code

    def intro(self):
        content = []
        for line in self._read(source=self.args.intro):
            content.append(line)
        content.append("")
        return content

    def jobs(self, *, content):
        for name, elem in self._collect(
            source=self.args.job,
            extensions=self.args.extensions,
        ):
            content.append("---")
            content.append(f"`{name}`")
            desc, code = self._gather(*self._read(source=elem))
            content.extend(desc)
            content.append("```yaml")
            content.extend(code)
            content.append("```")
            content.append("")
        return content

    def build(self, *, content):
        text = "\n".join(content)
        body = markdown.markdown(
            text=text,
            extensions=["fenced_code"],
            output_format="html",
        )
        html = HTML.substitute(title=self.args.title, body=body)

        base = os.path.dirname(self.args.output)
        if not os.path.exists(base):
            os.makedirs(base)

        with open(self.args.output, "w", encoding=ENCODING) as handle:
            handle.write(html)

    def __call__(self):
        content = self.intro()
        content = self.jobs(content=content)
        self.build(content=content)
        return 0


def main():
    args = arguments()

    code = Code(args=args)
    return code()


if __name__ == "__main__":
    sys.exit(main())
